import * as _ from 'lodash';
import { Token } from './token';
import { PathUtils } from './path-utils';

export class Tokenizer {
    static breakers = [160, 8220, 8222, 8211, 8593, 32, 10, 13];
    static punctuations = ['.', ',', ';'];

    static text(txt: string): object {
        const tokenized: object = {};

        txt += String.fromCharCode(0);
        const len = txt.length;
        let actType = null;
        let actString = '';
        for (let i = 0; i < len; ++i) {
            const c = txt.charCodeAt(i);
            let currType = Token.UNKNOWN;
            if (c === -1) {
                currType = '-1';
            } else if (_.indexOf(this.breakers, c) !== -1) {
                currType = Token.SPACE;
            } else if (c >= 65 && c <= 90) {
                currType = Token.WORD;
            } else if (c >= 97 && c <= 122) {
                currType = Token.WORD;
            } else if (c >= 48 && c <= 57) {
                currType = Token.NUMBER;
            } else if (_.indexOf(this.punctuations, String.fromCharCode(c)) !== -1 ) {
                currType = Token.PUNCTUATION;
            } else if (c >= 192) {
                currType = Token.WORD;
            }
            if (actType == null) {
                actType = currType;
            }
            if (actType === currType) {
                actString += String.fromCharCode(c);
                continue;
            }
            if (actType === Token.WORD) {
                tokenized[i] = actString;
            } else {
                tokenized[i] = {'c': actString};
                if (actType !== Token.UNKNOWN) {
                    tokenized[i]['t'] = actType;
                }
            }

            actString = String.fromCharCode(c);
            actType = currType;
        }
        const keys: string[] = _.keys(tokenized);
        keys.forEach((k: string) => {
            const d = new Token(tokenized[k])
            if (d.type === Token.SPACE) {
                delete tokenized[k];
            }
        });
        this._reindexMap(tokenized);
        return tokenized;
    }

    static markup(txt: string): object {
        const ret = {};
        const lines = txt.split('\n');
        let actString = '';
        for (let i = 0; i < lines.length; ++i) {
            const currString = lines[i].trim();
            if (currString.length === 0) {
                if (actString.length !== 0) {
                    const par = {};
                    par['t'] = Token.PARAGRAPH;
                    par['c'] = this.text(actString);
                    ret[i] = par;
                    actString = '';
                    continue;
                }
            } else {
                if (actString.length !== 0) {
                    actString += ' ' + currString;
                    continue;
                } else {
                    if (currString.substring(0, 2) === '# ') {
                        const par = {};
                        par['t'] = Token.HEADER1;
                        par['c'] = this.text(currString.substring(2));
                        ret[i] = par;
                        continue;
                    } else if (currString.substring(0, 3) === '## ') {
                        const par = {};
                        par['t'] = Token.HEADER2;
                        par['c'] = this.text(currString.substring(3));
                        ret[i] = par;
                        continue;
                    } else {
                        actString = currString;
                    }
                }
            }
        }
        if (actString.length !== 0) {
            const par = {};
            par['t'] = Token.PARAGRAPH;
            par['c'] = this.text(actString);
            ret[lines.length] = par;
        }
        this._reindexMap(ret);
        return ret;
    }
    private static _reindexMap(m: object): void {
        const keys = _.keys(m);
        const newKeys = new PathUtils(keys.length);
        for (let i = 0; i < keys.length; ++i) {
            m[newKeys.keyAt(i)] = m[keys[i]];
            delete m[keys[i]];
        }

    }

}
