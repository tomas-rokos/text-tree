import * as _ from 'lodash';

export class PathUtils {
    private power = 1;

    static join(...parts: string[]): string {
        const result: string[] = [];

        for (const part of parts) {
            if (part != null && part.length > 0) {
                result.push(part);
            }
        }
        return _.join(result, '.');
    }

    static isInRangeIdx(rangeIdx: string, keyToTest: string): boolean {
        const pos = rangeIdx.indexOf('-');
        if (pos === -1) {
            return false;
        }
        const first = rangeIdx.substring(0, pos);
        const last = rangeIdx.substring(pos + 1);
        if (keyToTest < first || keyToTest > last) {
            return false;
        }
        return true;
    }

    static createRangeIdx(first: string, last: string): string {
        let realFirst = first;
        let idxOf = first.indexOf('-');
        if (idxOf !== -1) {
            realFirst = first.substring(0, idxOf);
        }
        let realLast = last;
        idxOf = last.lastIndexOf('-');
        if (idxOf !== -1) {
            realLast = last.substring(idxOf + 1);
        }
        return realFirst + '-' + realLast;
    }

    static parentPath(path: string): string {
        if (path == null) {
            return '';
        }
        const idx = path.lastIndexOf('.');
        return idx !== -1 ? path.substring(0, idx) : '';
    }

    constructor(num?: number) {
        this.power = num > 1 ? Math.trunc(Math.log(num - 1) / Math.log(26)) + 1 : 1;
    }
    keyAt(idx: number): string {
        let res  = '';
        for (let i = 0; i < this.power; ++i) {
            const curr = idx % 26;
            idx = Math.trunc(idx / 26);
            res = String.fromCharCode(curr + 97) + res;
        }
        return res;
    }
}
