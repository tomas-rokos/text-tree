import * as _ from 'lodash';
import { Token } from './token';
import { PathUtils } from './path-utils';
import { FragmentQuery } from './fragment.query';

export interface FragmentQueryResult {
    tokenPath: string;
}

export interface FragmentQueryParameters {
    allLevels?: boolean;
    atomicTokenTypes?: string[];
}

export class Fragment {
    dataRoot: string;
    data: object;

    constructor(val: object, path?: string) {
        this.data = val;
        this.dataRoot = path;
    }

    getAsToken(path: string): Token {
        return new Token(_.get(this.data, path), PathUtils.join(this.dataRoot, path));
    }

    query(params?: FragmentQueryParameters): IterableIterator<FragmentQueryResult> {
        return new FragmentQuery(this, params);
    }

    deltaFlatten(keyPath = ''): { [key: string]: any} {
        const fullPath = PathUtils.join( this.dataRoot, keyPath);
        const delta = {};
        delta[fullPath] =  null;
        const parentPath = PathUtils.parentPath(fullPath);
        _.forEach(_.get(this.data, PathUtils.join(keyPath, 'c'), {}), (val: any, key: string) => {
            delta[PathUtils.join(parentPath, key)] = val;
        });

        return delta;
    }
    deltaNest(first: string, last: string, newType: string, subPath = ''): { [key: string]: any} {
        if (first === last) {
            return this.getAsToken(PathUtils.join(subPath, first)).deltaSetProperties({'t': newType});
        }
        const delta = {};
        const nested = {'c': {}, 't': newType};
        const rangeKey = PathUtils.createRangeIdx(first, last);
        delta[PathUtils.join(this.dataRoot, subPath, rangeKey)] = nested;
        const keys = _.keys(subPath === '' ? this.data : _.get(this.data, subPath, {}));
        for (const key of keys) {
            if (PathUtils.isInRangeIdx(rangeKey, key) === false) {
                continue;
            }
            nested['c'][key] = _.get(this.data, PathUtils.join(subPath, key));
            delta[PathUtils.join(this.dataRoot, subPath, key)] = null;
        }
        return delta;
    }
    validatePath(path: string): string {
        const parts = path.split('.');
        const result: string[] = [];
        let curr = 0;
        let m: object = this.data;
        while (curr < parts.length) {
            const currPart = parts[curr];
            if (_.has(m, currPart) === true) {
                result.push(currPart);
                if (++curr < parts.length) {
                    m = m[currPart];
                }
                continue;
            }

            const keys = _.keys(m);
            let found = false;
            for (let i = 0; i < keys.length; ++i) {
                const currKey = keys[i];
                if (PathUtils.isInRangeIdx(currKey, currPart)) {
                    result.push(currKey);
                    result.push('c');
                    m = m[currKey]['c'];
                    found = true;
                    break;
                }
            }
            if (found) {
                continue;
            }
            return null;
        }
        return result.join('.');
    }

}
