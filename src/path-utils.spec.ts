import * as _ from 'lodash';
import { PathUtils } from './path-utils';
import { expect } from 'chai';

const testingData = require('./sample.json');

describe('PathUtils', () => {
    it('lodash - set on path', () => {
        const o = {};
        expect(_.set(o, 'aa', 3)).to.deep.equal({'aa': 3});
        expect(o).to.deep.equal({'aa': 3});

        expect(_.set({}, 'a.b', 'x')).to.deep.equal({'a': {'b': 'x'}});
        expect(_.set({}, 'a.b-e', 'x')).to.deep.equal({'a': {'b-e': 'x'}});
        expect(_.set(o, 'aa.b-e', 'x')).to.deep.equal({'aa': {'b-e': 'x'}});
        expect(_.set(o, 'a.b', {'x': 'y'})).to.deep.equal({'aa': {'b-e': 'x'}, 'a': {'b': {'x': 'y'}}});
    });
    it('lodash - get on path', () => {
        expect(_.get('a', 'z', '-')).to.deep.equal('-');
        expect(_.get('a', null, '-')).to.deep.equal('-');
        expect(_.get('a', '', '-')).to.deep.equal('-');

        expect(_.get({'a': 'b'}, '')).to.equal(undefined);

        expect(_.get(testingData, 'samples.simple.doc.a.t')).to.deep.equal('p');
        expect(_.get(testingData, 'samples.simple.doc.a.z')).to.deep.equal(undefined);
    });
    it('.join - no value', () => {
        expect(PathUtils.join(null)).to.equal('');
    });
    it('.join - single value', () => {
        expect(PathUtils.join('a')).to.equal('a');
    });
    it('.join - more values', () => {
        expect(PathUtils.join('a', 'b')).to.equal('a.b');
        expect(PathUtils.join('a', 'b', 'c')).to.equal('a.b.c');
        expect(PathUtils.join('a', 'b', 'c', 'd')).to.equal('a.b.c.d');
        expect(PathUtils.join('a', 'b', 'c', 'd', 'e')).to.equal('a.b.c.d.e');
    });
    it('.join - skipped values', () => {
        expect(PathUtils.join(null, 'b')).to.equal('b');
        expect(PathUtils.join('a', null, 'c')).to.equal('a.c');
        expect(PathUtils.join('a', null, null, 'd')).to.equal('a.d');
        expect(PathUtils.join('a', null, 'c', null, 'e')).to.equal('a.c.e');
    });
    it('.join - empty values', () => {
        expect(PathUtils.join('', 'b')).to.equal('b');
        expect(PathUtils.join('a', '', 'c')).to.equal('a.c');
        expect(PathUtils.join('a', '', '', 'd')).to.equal('a.d');
        expect(PathUtils.join('a', '', 'c', '', 'e')).to.equal('a.c.e');
    });
    it('.keyAt - <26', () => {
        const k: PathUtils = new PathUtils(20);
        expect(k.keyAt(0)).to.equal('a');
        expect(k.keyAt(1)).to.equal('b');
        expect(k.keyAt(2)).to.equal('c');
        expect(k.keyAt(3)).to.equal('d');
        expect(k.keyAt(4)).to.equal('e');
        expect(k.keyAt(5)).to.equal('f');
        expect(k.keyAt(6)).to.equal('g');
        expect(k.keyAt(7)).to.equal('h');
        expect(k.keyAt(8)).to.equal('i');
        expect(k.keyAt(9)).to.equal('j');
        expect(k.keyAt(10)).to.equal('k');
        expect(k.keyAt(11)).to.equal('l');
        expect(k.keyAt(12)).to.equal('m');
        expect(k.keyAt(13)).to.equal('n');
        expect(k.keyAt(14)).to.equal('o');
        expect(k.keyAt(15)).to.equal('p');
        expect(k.keyAt(16)).to.equal('q');
        expect(k.keyAt(17)).to.equal('r');
        expect(k.keyAt(18)).to.equal('s');
        expect(k.keyAt(19)).to.equal('t');
        expect(k.keyAt(20)).to.equal('u');
        expect(k.keyAt(21)).to.equal('v');
        expect(k.keyAt(22)).to.equal('w');
        expect(k.keyAt(23)).to.equal('x');
        expect(k.keyAt(24)).to.equal('y');
        expect(k.keyAt(25)).to.equal('z');
    });
    it('.keyAt - ==26', () => {
        const k: PathUtils = new PathUtils(26);
        expect(k.keyAt(0)).to.equal('a');
    });
    it('.keyAt - ==27', () => {
        const k: PathUtils = new PathUtils(27);
        expect(k.keyAt(0)).to.equal('aa');
        expect(k.keyAt(1)).to.equal('ab');
        expect(k.keyAt(26)).to.equal('ba');
    });
    it('.keyAt - >26 && <=676', () => {
        const k: PathUtils = new PathUtils(600);
        expect(k.keyAt(0)).to.equal('aa');
        expect(k.keyAt(1)).to.equal('ab');
        expect(k.keyAt(2)).to.equal('ac');
        expect(k.keyAt(3)).to.equal('ad');
        expect(k.keyAt(4)).to.equal('ae');
        expect(k.keyAt(5)).to.equal('af');
        expect(k.keyAt(6)).to.equal('ag');
        expect(k.keyAt(7)).to.equal('ah');
        expect(k.keyAt(8)).to.equal('ai');
        expect(k.keyAt(9)).to.equal('aj');
        expect(k.keyAt(10)).to.equal('ak');
        expect(k.keyAt(11)).to.equal('al');
        expect(k.keyAt(12)).to.equal('am');
        expect(k.keyAt(13)).to.equal('an');
        expect(k.keyAt(14)).to.equal('ao');
        expect(k.keyAt(15)).to.equal('ap');
        expect(k.keyAt(16)).to.equal('aq');
        expect(k.keyAt(17)).to.equal('ar');
        expect(k.keyAt(18)).to.equal('as');
        expect(k.keyAt(19)).to.equal('at');
        expect(k.keyAt(20)).to.equal('au');
        expect(k.keyAt(21)).to.equal('av');
        expect(k.keyAt(22)).to.equal('aw');
        expect(k.keyAt(23)).to.equal('ax');
        expect(k.keyAt(24)).to.equal('ay');
        expect(k.keyAt(25)).to.equal('az');
        expect(k.keyAt(26)).to.equal('ba');
    });
    it('.keyAt - >676 && <=17576', () => {
        const k: PathUtils = new PathUtils(1000);
        expect(k.keyAt(0)).to.equal('aaa');
        expect(k.keyAt(1)).to.equal('aab');
        expect(k.keyAt(26)).to.equal('aba');
        expect(k.keyAt(676)).to.equal('baa');
    });
    it('.createRangeIdx', () => {
        expect(PathUtils.createRangeIdx('a', 'b')).to.equal('a-b');
        expect(PathUtils.createRangeIdx('aa', 'ab')).to.equal('aa-ab');
        expect(PathUtils.createRangeIdx('aa-am', 'an')).to.equal('aa-an');
        expect(PathUtils.createRangeIdx('aa-am', 'an-aq')).to.equal('aa-aq');
        expect(PathUtils.createRangeIdx('am', 'an-aq')).to.equal('am-aq');
    });
    it('.isInRangeIdx', () => {
        expect(PathUtils.isInRangeIdx('ac-ae', 'aa')).to.equal(false);
        expect(PathUtils.isInRangeIdx('ac-ae', 'ab')).to.equal(false);
        expect(PathUtils.isInRangeIdx('ac-ae', 'ac')).to.equal(true);
        expect(PathUtils.isInRangeIdx('ac-ae', 'ad')).to.equal(true);
        expect(PathUtils.isInRangeIdx('ac-ae', 'ae')).to.equal(true);
        expect(PathUtils.isInRangeIdx('ac-ae', 'af')).to.equal(false);
    });
    it('.parentPath', () => {
        expect(PathUtils.parentPath(null)).to.equal('');
        expect(PathUtils.parentPath('')).to.equal('');
        expect(PathUtils.parentPath('aaa')).to.equal('');
        expect(PathUtils.parentPath('a.b')).to.equal('a');
        expect(PathUtils.parentPath('aaa.bbb')).to.equal('aaa');
        expect(PathUtils.parentPath('a.b.c')).to.equal('a.b');
        expect(PathUtils.parentPath('aaa.bbb.ccc')).to.equal('aaa.bbb');
    })
});
