import * as _ from 'lodash';
import { Token } from './token';
import { Fragment } from './fragment';
import { expect } from 'chai';

const testingData = require('./sample.json');

describe('Token', () => {
    it('.type - null', () => {
        expect(new Token(null).type).to.equal(Token.WORD);
    });
    it('.type - string', () => {
        expect(new Token('test').type).to.equal(Token.WORD);
    });
    it('.type - map without type', () => {
        expect(new Token({'c': 'test'}).type).to.equal(Token.WORD);
    });
    it('.type - map with type', () => {
        expect(new Token({'c': 'test', 't': '.'}).type).to.equal(Token.PUNCTUATION);
    });
    it('.contentString - null', () => {
        expect(new Token(null).contentString).to.equal('');
    });
    it('.contentString - string', () => {
        expect(new Token('test').contentString).to.equal('test');
    });
    it('.contentString - int', () => {
        expect(new Token(3).contentString).to.equal('3');
    });
    it('.contentString - double', () => {
        expect(new Token(3.14).contentString).to.equal('3.14');
    });
    it('.contentString - nested content', () => {
        expect(new Token({'c': 'test'}).contentString).to.equal('test');
    });
    it('.contentString - nested int content', () => {
        expect(new Token({'c': 20}).contentString).to.equal('20');
    });
    it('.contentString - nested text tree content', () => {
        expect(new Token({'c': {'a': 'alpha', 'b': 'beta'}}).contentString).to.equal('alpha beta');
    });
    it('.isLeaf - null', () => {
        expect(new Token(null).isLeaf).to.equal(true);
    });
    it('.isLeaf - string', () => {
        expect(new Token('test').isLeaf).to.equal(true);
    });
    it('.isLeaf - int', () => {
        expect(new Token(3).isLeaf).to.equal(true);
    });
    it('.isLeaf - nested content', () => {
        expect(new Token({'c': 'test'}).isLeaf).to.equal(true);
    });
    it('.isLeaf - nested int content', () => {
        expect(new Token({'c': 20}).isLeaf).to.equal(true);
    });
    it('.isLeaf - nested text tree content', () => {
        expect(new Token({'c': {'a': 'alpha', 'b': 'beta'}}).isLeaf).to.equal(false);
    });
    it('.deltaSetProperties - string token, parent level, one property', () => {
        const g: Token = new Token(testingData['samples']['simple']['doc']['b']['c'], 'samples.simple.doc.b.c');
        const deltas: { [key: string]: any} = g.deltaSetProperties({'b.t': 'k'});
        expect(_.size(deltas)).to.equal(2);
        expect(_.keys(deltas)[0]).to.equal('samples.simple.doc.b.c.b.c');
        expect(_.values(deltas)[0]).to.equal('ja');
        expect(_.keys(deltas)[1]).to.equal('samples.simple.doc.b.c.b.t');
        expect(_.values(deltas)[1]).to.equal('k');
    });
    it('.deltaSetProperties - string token, direct level, one property', () => {
        const g: Token = new Token(testingData['samples']['simple']['doc']['b']['c']['b'], 'samples.simple.doc.b.c.b');
        const deltas: { [key: string]: any} = g.deltaSetProperties({'t': 'k'});
        expect(_.size(deltas)).to.equal(2);
        expect(_.keys(deltas)[0]).to.equal('samples.simple.doc.b.c.b.c');
        expect(_.values(deltas)[0]).to.equal('ja');
        expect(_.keys(deltas)[1]).to.equal('samples.simple.doc.b.c.b.t');
        expect(_.values(deltas)[1]).to.equal('k');
    });
    it('.deltaSetProperties - string token, direct level, two properties', () => {
        const g: Token = new Token(testingData['samples']['simple']['doc']['b']['c']['b'], 'samples.simple.doc.b.c.b');
        const deltas: { [key: string]: any} = g.deltaSetProperties({'t': 'k', 'p': 'i'});
        expect(_.size(deltas)).to.equal(3);
        expect(_.keys(deltas)[0]).to.equal('samples.simple.doc.b.c.b.c');
        expect(_.values(deltas)[0]).to.equal('ja');
        expect(_.keys(deltas)[1]).to.equal('samples.simple.doc.b.c.b.t');
        expect(_.values(deltas)[1]).to.equal('k');
        expect(_.keys(deltas)[2]).to.equal('samples.simple.doc.b.c.b.p');
        expect(_.values(deltas)[2]).to.equal('i');
    });
    it('.deltaSetProperties - map token, parent level, one property', () => {
        const g: Token = new Token(testingData['samples']['simple']['doc']['b']['c'], 'samples.simple.doc.b.c');
        const deltas: { [key: string]: any} = g.deltaSetProperties({'e.t': 'k'});
        expect(_.size(deltas)).to.equal(1);
        expect(_.keys(deltas)[0]).to.equal('samples.simple.doc.b.c.e.t');
        expect(_.values(deltas)[0]).to.equal('k');
    });
    it('.deltaSetProperties - map token, direct level, one property', () => {
        const g: Token = new Token(testingData['samples']['simple']['doc']['b']['c']['e'], 'samples.simple.doc.b.c.e');
        const deltas: { [key: string]: any} = g.deltaSetProperties({'t': 'k'});
        expect(_.size(deltas)).to.equal(1);
        expect(_.keys(deltas)[0]).to.equal('samples.simple.doc.b.c.e.t');
        expect(_.values(deltas)[0]).to.equal('k');
    });
    it('.contentFragment', () => {
        const g: Token = new Token(testingData['samples']['simple']['doc']['b'], 'samples.simple.doc.b');
        const f: Fragment = g.contentFragment;
        expect(f.dataRoot).to.equal('samples.simple.doc.b.c');
    });
    it('.contentFragment - leaf token', () => {
        const g: Token = new Token(testingData['samples']['simple']['doc']['b']['c']['e'], 'samples.simple.doc.b.c.e');
        const f: Fragment = g.contentFragment;
        expect(f).to.equal(null);
    });
});
