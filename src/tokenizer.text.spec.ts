import { Tokenizer } from './tokenizer';
import { Token } from './token';
import { expect } from 'chai';
import * as _ from 'lodash';

describe('Tokenizer.text', () => {
    it('length', () => {
        const words = Tokenizer.text('This is\n1st test');
        expect(_.size(words)).to.equal(5);
    });
    it('length - single character at the end', () => {
        const words = Tokenizer.text('This is\n1st t');
        expect(_.size(words)).to.equal(5);
    });
    it('length - space at the end', () => {
        const words = Tokenizer.text('This is\n1st ');
        expect(_.size(words)).to.equal(4);
    });
    it('length - multispace at the end', () => {
        const words = Tokenizer.text('This is\n1st   ');
        expect(_.size(words)).to.equal(4);
    });
    it('words content', () => {
        const words = Tokenizer.text('This is\n1st test');
        expect(words['a']).to.equal('This');
        expect(words['b']).to.equal('is');
        expect(words['c']).to.deep.equal({'t': 'n', 'c': '1'});
        expect(words['d']).to.equal('st');
        expect(words['e']).to.equal('test');
    });
    it('multispaces', () => {
        const words = Tokenizer.text('This   is\n1st    test');
        expect(words['a']).to.equal('This');
        expect(words['b']).to.equal('is');
        expect(words['c']).to.deep.equal({'t': 'n', 'c': '1'});
        expect(words['d']).to.equal('st');
        expect(words['e']).to.equal('test');
    });
    it('special chars', () => {
        const words = Tokenizer.text('Tohle je \'věta\' s extra, značkama. ');
        expect(words['a']).to.equal('Tohle');
        expect(words['b']).to.equal('je');
        expect(words['c']).to.deep.equal({'c': '\''});
        expect(words['d']).to.equal('věta');
        expect(words['e']).to.deep.equal({'c': '\''});
        expect(words['f']).to.equal('s');
        expect(words['g']).to.equal('extra');
        expect(words['h']).to.deep.equal({'t': '.', 'c': ','});
        expect(words['i']).to.equal('značkama');
        expect(words['j']).to.deep.equal({'t': '.', 'c': '.'});
    });
});
