import * as _ from 'lodash';
import { Token } from './token';
import { Fragment, FragmentQueryParameters, FragmentQueryResult } from './fragment';
import { PathUtils } from './path-utils';

export class FragmentQuery implements IterableIterator<FragmentQueryResult> {
    f: Fragment;
    params: FragmentQueryParameters;
    private nestedIters: object[];
    query: object[];

    constructor (f: Fragment, params: FragmentQueryParameters) {
        this.f = f;
        this.params = params || {};
//        this.query = this.parseQuery(this.params.query);
    }
    [Symbol.iterator](): IterableIterator<FragmentQueryResult> {
        return this;
    }

    public next(): IteratorResult<FragmentQueryResult> {
        if (this.nestedIters == null) {
            this.nestedIters = [];
            this.nestedIters.push({'keys': _.keys(this.f.data), 'idx': -1});
        }
        if (this.nestedIters.length === 0) {
            return {'value': null, done: true};
        }
        const nestedItersLast = _.last(this.nestedIters);
        nestedItersLast['idx'] = nestedItersLast['idx'] + 1;
        if (nestedItersLast['idx'] === nestedItersLast['keys'].length) {
            this.nestedIters.pop();
            return this.next();
        }
        const currKey = PathUtils.join(nestedItersLast['path'], nestedItersLast['keys'][nestedItersLast['idx']]);
        if (this.params.allLevels === true) {
            const t = this.f.getAsToken(currKey);
            if (t.isLeaf === false && (this.params.atomicTokenTypes == null || this.params.atomicTokenTypes.indexOf(t.type) === -1)) {
                const cf = t.contentFragment;
                this.nestedIters.push({'path': PathUtils.join(currKey, 'c'), 'keys': _.keys(cf.data), 'idx': -1});
                return this.next();
            }
        }

        // String dbEnd = currKey;
        // for (int i=0;i<query.length;++i) {
        //     if (nestedItersLast['idx']+i >= nestedItersLast['keys'].length)
        //         return moveNext();
        //     String testKey = JoinPath(nestedItersLast['path'],nestedItersLast['keys'][nestedItersLast['idx']+i]);
        //     dbEnd = testKey;
        //     String match = validateQueryToken(testKey, query[i]);
        //     if (match == null)
        //         return moveNext();
        // }

        return {value: {tokenPath: currKey}, done: false};
    }
    private parseQuery(q: object): object[] {
        const ret: object[] = [];
        // if (q == null)
        //     return ret;
        // if (q['t'] != null && q['t'].compareTo(Token.QUERY) == 0) {
        //     Map qc = q['c'];
        //     qc.forEach((String key, dynamic val) {
        //         if (val is String)
        //             ret.add({'c':val});
        //         else
        //             ret.add(parseQueryToken(val));
        //     });
        // } else {
        //     ret.add(parseQueryToken(q));
        // }
        return ret;
    }
    private parseQueryToken(token: object): object {
        return token;
    }
    private validateQueryToken(currKey: string, queryToken: object): string {
        // Token t = f.asToken(currKey);
        // if (queryToken['c'] != null)  {
        //     if (queryToken['c'] is String && t.content.compareTo(queryToken['c']) != 0)
        //         return null;
        //     if (queryToken['c'] is Map) {
        //         Map qc = queryToken['c'];
        //         if (qc.containsValue(t.content) == false)
        //             return null;
        //     }
        // }
        // if (queryToken['m'] != null)  {
        //     if (queryToken['m'] is String && t.content.compareTo(queryToken['m']) != 0)
        //         return null;
        //     if (queryToken['m'] is Map) {
        //         Map qc = queryToken['m'];
        //         List<String> mws = t.masterWords;
        //         if (mws.isEmpty)
        //             mws.add(t.content);
        //         bool found = false;
        //         mws.forEach((String mw) {
        //             if (qc.containsValue(mw) == true)
        //                 found = true;
        //         });
        //         if (found == false)
        //             return null;
        //     }
        // }
        // if (queryToken['t'] != null && t.type.compareTo(queryToken['t']) != 0)
        //     return null;
        return '';
    }
}
