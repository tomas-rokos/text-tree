import { Tokenizer } from './tokenizer';
import { Token } from './token';
import { expect } from 'chai';
import * as _ from 'lodash';

describe('Tokenizer.markup', () => {
    it('single line text', () => {
        const doc = Tokenizer.markup('Jedna řádka textu v Markupu.');
        expect(_.size(doc)).to.equal(1);
        expect(doc['a']['t']).to.equal(Token.PARAGRAPH);
        expect(typeof doc['a']['c']).to.equal('object');
        expect(_.size(doc['a']['c'])).to.equal(6);
    });
    it('single paragraph with multiline text', () => {
        const doc = Tokenizer.markup('Jedna řádka textu\nv Markupu.');
        expect(_.size(doc)).to.equal(1);
        expect(doc['a']['t']).to.equal(Token.PARAGRAPH);
        expect(typeof doc['a']['c']).to.equal('object');
        expect(_.size(doc['a']['c'])).to.equal(6);
    });
    it('two paragraphs with single line text', () => {
        const doc = Tokenizer.markup('Jedna řádka textu\n\nv Markupu.');
        expect(_.size(doc)).to.equal(2);
        expect(doc['a']['t']).to.equal(Token.PARAGRAPH);
        expect(typeof doc['a']['c']).to.equal('object');
        expect(_.size(doc['a']['c'])).to.equal(3);
        expect(doc['b']['t']).to.equal(Token.PARAGRAPH);
        expect(typeof doc['b']['c']).to.equal('object');
        expect(_.size(doc['b']['c'])).to.equal(3);
    });
    it('header and two paragraphs with multiline text', () => {
        const doc = Tokenizer.markup('# titulek\nPrvní odstavec\ntextu ve kterém\n\njsou dva \nodstavce.');
        expect(_.size(doc)).to.equal(3);
        expect(doc['a']['t']).to.equal(Token.HEADER1);
        expect(typeof doc['a']['c']).to.equal('object');
        expect(_.size(doc['a']['c'])).to.equal(1);
        expect(doc['a']['c']['a']).to.equal('titulek');
        expect(doc['b']['t']).to.equal(Token.PARAGRAPH);
        expect(typeof doc['b']['c']).to.equal('object');
        expect(_.size(doc['b']['c'])).to.equal(5);
        expect(doc['c']['t']).to.equal(Token.PARAGRAPH);
        expect(typeof doc['c']['c']).to.equal('object');
        expect(_.size(doc['c']['c'])).to.equal(4);
    });
    it('paragraphs which has subsequent line starting with hash', () => {
        const doc = Tokenizer.markup('# titulek\nPrvní odstavec\n# textu ve kterém\n\njsou dva \nodstavce.');
        expect(_.size(doc)).to.equal(3);
        expect(doc['a']['t']).to.equal(Token.HEADER1);
        expect(typeof doc['a']['c']).to.equal('object');
        expect(_.size(doc['a']['c'])).to.equal(1);
        expect(doc['a']['c']['a']).to.equal('titulek');
        expect(doc['b']['t']).to.equal(Token.PARAGRAPH);
        expect(typeof doc['b']['c']).to.equal('object');
        expect(_.size(doc['b']['c'])).to.equal(6);
        expect(doc['c']['t']).to.equal(Token.PARAGRAPH);
        expect(typeof doc['c']['c']).to.equal('object');
        expect(_.size(doc['c']['c'])).to.equal(4);
    });
    it('hader1 and header2', () => {
        const doc = Tokenizer.markup('# titulek\nPrvní odstavec\n# textu ve kterém\n\n## druhy\njsou dva \nodstavce.');
        expect(_.size(doc)).to.equal(4);
        expect(doc['a']['t']).to.equal(Token.HEADER1);
        expect(typeof doc['a']['c']).to.equal('object');
        expect(_.size(doc['a']['c'])).to.equal(1);
        expect(doc['a']['c']['a']).to.equal('titulek');
        expect(doc['b']['t']).to.equal(Token.PARAGRAPH);
        expect(typeof doc['b']['c']).to.equal('object');
        expect(_.size(doc['b']['c'])).to.equal(6);
        expect(doc['c']['t']).to.equal(Token.HEADER2);
        expect(typeof doc['c']['c']).to.equal('object');
        expect(_.size(doc['c']['c'])).to.equal(1);
        expect(doc['c']['c']['a']).to.equal('druhy');
        expect(doc['d']['t']).to.equal(Token.PARAGRAPH);
        expect(typeof doc['d']['c']).to.equal('object');
        expect(_.size(doc['d']['c'])).to.equal(4);
    });
    it('hader1 and header2 with lot of empty lines', () => {
        const doc = Tokenizer.markup('\n\n# titulek\n\nPrvní odstavec\n# textu ve kterém\n\n\n\n## druhy\n\n\njsou dva \nodstavce.\n\n\n');
        expect(_.size(doc)).to.equal(4);
        expect(doc['a']['t']).to.equal(Token.HEADER1);
        expect(typeof doc['a']['c']).to.equal('object');
        expect(_.size(doc['a']['c'])).to.equal(1);
        expect(doc['a']['c']['a']).to.equal('titulek');
        expect(doc['b']['t']).to.equal(Token.PARAGRAPH);
        expect(typeof doc['b']['c']).to.equal('object');
        expect(_.size(doc['b']['c'])).to.equal(6);
        expect(doc['c']['t']).to.equal(Token.HEADER2);
        expect(typeof doc['c']['c']).to.equal('object');
        expect(_.size(doc['c']['c'])).to.equal(1);
        expect(doc['c']['c']['a']).to.equal('druhy');
        expect(doc['d']['t']).to.equal(Token.PARAGRAPH);
        expect(typeof doc['d']['c']).to.equal('object');
        expect(_.size(doc['d']['c'])).to.equal(4);
    });
});
