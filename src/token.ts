import * as _ from 'lodash';

import { PathUtils } from './path-utils';
import { Fragment } from './fragment';

export class Token {
    static readonly UNKNOWN = '';
    static readonly WORD = 'w';
    static readonly NUMBER = 'n';
    static readonly SPACE = '-';
    static readonly PUNCTUATION = '.';

    static readonly PARAGRAPH = 'p';
    static readonly HEADER1 = 'h1';
    static readonly HEADER2 = 'h2';

    static readonly SENTENCE = 's';

    static readonly KEY = 'k';
    static readonly MARK = 'm';

    static readonly QUERY = 'q';

    data: any;
    dataRoot: string;

    constructor(val: any, path?: string) {
        this.data = val;
        this.dataRoot = path;
    }

    get type(): string {
        return _.get(this.data, 't', Token.WORD);
    }

    get contentFragment(): Fragment {
        if (this.isLeaf) {
            return null;
        }
        return new Fragment(_.get(this.data, 'c'), PathUtils.join(this.dataRoot, 'c'));
    }

    get contentString(): string {
        return this._content(this.data);
    }
    private _content(val: any): string {
        if (val == null) {
            return '';
        }
        if (typeof val === 'object') {
            const subv = val['c'];
            if (typeof subv === 'object') {
                let result = '';
                _.forEach(subv, (subvv, subvk) => {
                    result += this._content(subvv) + ' ';
                });
                return result.trim();
            } else {
                return this._content(subv);
            }
        } else {
            return val.toString();
        }
    }

    get isLeaf(): boolean {
        const val = _.get(this.data, 'c');
        if (val == null) {
            return true;
        }
        return typeof val === 'object' ? false : true;
    }

    deltaSetProperties(propertyMap: { [key: string]: any} ): { [key: string]: any} {
        const delta: { [key: string]: any} = {};
        _.forEach(propertyMap, (val: any, key: string) => {
            const parentPath = PathUtils.parentPath(key);
            const parentVal = parentPath === '' ? this.data : _.get(this.data, parentPath);
            if (typeof parentVal === 'string') {
                delta[PathUtils.join(this.dataRoot, parentPath, 'c')] = parentVal;
            }
            delta[PathUtils.join(this.dataRoot, key)] = val;
        });
        return delta;
    }
}
