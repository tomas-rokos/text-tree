import * as _ from 'lodash';
import { Fragment } from './fragment';
import { Token } from './token';
import { expect } from 'chai';

const testingData = require('./sample.json');

describe('Fragment', () => {
    it('.getAsToken', () => {
        const f: Fragment = new Fragment(testingData['samples']['simple']['doc'], 'samples.simple.doc');
        const t: Token = f.getAsToken('a.c.a-e');
        expect(t.dataRoot).to.equal('samples.simple.doc.a.c.a-e');
        expect(t.type).to.equal(Token.SENTENCE);
        expect(t.contentString).to.equal('a b c d e');
    });

    it('.deltaNest - direct level', () => {
        const g: Fragment = new Fragment(testingData['samples']['simple']['doc']['a']['c']['a-e']['c'], 'a.c.a-e.c');
        const deltas = g.deltaNest('b', 'd', 's');
        expect(_.size(deltas)).to.equal(4);
        expect(_.keys(deltas)[0]).to.equal('a.c.a-e.c.b-d');
        expect(_.values(deltas)[0]).to.deep.equal({'c': {'b': 'b', 'c': 'c', 'd': 'd'}, 't': 's'});
        expect(_.keys(deltas)[1]).to.equal('a.c.a-e.c.b');
        expect(_.values(deltas)[1]).to.equal(null);
        expect(_.keys(deltas)[2]).to.equal('a.c.a-e.c.c');
        expect(_.values(deltas)[2]).to.equal(null);
        expect(_.keys(deltas)[3]).to.equal('a.c.a-e.c.d');
        expect(_.values(deltas)[3]).to.equal(null);
    });
    it('.deltaNest - nested level', () => {
        const g: Fragment = new Fragment(testingData['samples']['simple']['doc']['a']['c']['a-e'], 'a.c.a-e');
        const deltas = g.deltaNest('b', 'd', 's', 'c');
        expect(_.size(deltas)).to.equal(4);
        expect(_.keys(deltas)[0]).to.equal('a.c.a-e.c.b-d');
        expect(_.values(deltas)[0]).to.deep.equal({'c': {'b': 'b', 'c': 'c', 'd': 'd'}, 't': 's'});
        expect(_.keys(deltas)[1]).to.equal('a.c.a-e.c.b');
        expect(_.values(deltas)[1]).to.equal(null);
        expect(_.keys(deltas)[2]).to.equal('a.c.a-e.c.c');
        expect(_.values(deltas)[2]).to.equal(null);
        expect(_.keys(deltas)[3]).to.equal('a.c.a-e.c.d');
        expect(_.values(deltas)[3]).to.equal(null);
    });
    it('.deltaNest - root level', () => {
        const g: Fragment = new Fragment(testingData['samples']['simple']['doc']);
        const deltas = g.deltaNest('b', 'd', 's', 'a.c.a-e.c');
        expect(_.size(deltas)).to.equal(4);
        expect(_.keys(deltas)[0]).to.equal('a.c.a-e.c.b-d');
        expect(_.values(deltas)[0]).to.deep.equal({'c': {'b': 'b', 'c': 'c', 'd': 'd'}, 't': 's'});
        expect(_.keys(deltas)[1]).to.equal('a.c.a-e.c.b');
        expect(_.values(deltas)[1]).to.equal(null);
        expect(_.keys(deltas)[2]).to.equal('a.c.a-e.c.c');
        expect(_.values(deltas)[2]).to.equal(null);
        expect(_.keys(deltas)[3]).to.equal('a.c.a-e.c.d');
        expect(_.values(deltas)[3]).to.equal(null);
    });
    it('.deltaNest - single string token ', () => {
        const g: Fragment = new Fragment(testingData['samples']['simple']['doc']['a']['c']['a-e']['c'], 'a.c.a-e.c');
        const deltas = g.deltaNest('b', 'b', 's');
        expect(_.size(deltas)).to.equal(2);
        expect(_.keys(deltas)[0]).to.equal('a.c.a-e.c.b.c');
        expect(_.values(deltas)[0]).to.equal('b');
        expect(_.keys(deltas)[1]).to.equal('a.c.a-e.c.b.t');
        expect(_.values(deltas)[1]).to.equal('s');
    });
    it('.deltaNest - single map token ', () => {
        const g: Fragment = new Fragment(testingData['samples']['simple']['doc']['b']['c'], 'b.c');
        const deltas = g.deltaNest('e', 'e', 's');
        expect(_.size(deltas)).to.equal(1);
        expect(_.keys(deltas)[0]).to.equal('b.c.e.t');
        expect(_.values(deltas)[0]).to.equal('s');
    });
    it('.deltaNest - single map indirect token ', () => {
        const g: Fragment = new Fragment(testingData['samples']['simple']['doc']['b'], 'b');
        const deltas = g.deltaNest('e', 'e', 's', 'c');
        expect(_.size(deltas)).to.equal(1);
        expect(_.keys(deltas)[0]).to.equal('b.c.e.t');
        expect(_.values(deltas)[0]).to.equal('s');
    });
    it('.deltaFlatten - direct level', () => {
        const g: Fragment = new Fragment(testingData['samples']['simple']['doc']['a']['c'], 'a.c');
        const deltas = g.deltaFlatten('a-e');
        expect(_.size(deltas)).to.equal(6);
        expect(_.keys(deltas)[0]).to.equal('a.c.a-e');
        expect(_.values(deltas)[0]).to.equal(null);
        expect(_.keys(deltas)[1]).to.equal('a.c.a');
        expect(_.values(deltas)[1]).to.equal('a');
        expect(_.keys(deltas)[2]).to.equal('a.c.b');
        expect(_.values(deltas)[2]).to.equal('b');
        expect(_.keys(deltas)[3]).to.equal('a.c.c');
        expect(_.values(deltas)[3]).to.equal('c');
        expect(_.keys(deltas)[4]).to.equal('a.c.d');
        expect(_.values(deltas)[4]).to.equal('d');
        expect(_.keys(deltas)[5]).to.equal('a.c.e');
        expect(_.values(deltas)[5]).to.equal('e');
    });
    it('.deltaFlatten - root Fragment', () => {
        const g: Fragment = new Fragment(testingData['samples']['simple']['doc']);
        const deltas = g.deltaFlatten('a.c.a-e');
        expect(_.size(deltas)).to.equal(6);
        expect(_.keys(deltas)[0]).to.equal('a.c.a-e');
        expect(_.values(deltas)[0]).to.equal(null);
        expect(_.keys(deltas)[1]).to.equal('a.c.a');
        expect(_.values(deltas)[1]).to.equal('a');
        expect(_.keys(deltas)[2]).to.equal('a.c.b');
        expect(_.values(deltas)[2]).to.equal('b');
        expect(_.keys(deltas)[3]).to.equal('a.c.c');
        expect(_.values(deltas)[3]).to.equal('c');
        expect(_.keys(deltas)[4]).to.equal('a.c.d');
        expect(_.values(deltas)[4]).to.equal('d');
        expect(_.keys(deltas)[5]).to.equal('a.c.e');
        expect(_.values(deltas)[5]).to.equal('e');
    });
    it('.deltaFlatten - nested level', () => {
        const g: Fragment = new Fragment(testingData['samples']['simple']['doc']['a'], 'a');
        const deltas = g.deltaFlatten('c.a-e');
        expect(_.size(deltas)).to.equal(6);
        expect(_.keys(deltas)[0]).to.equal('a.c.a-e');
        expect(_.values(deltas)[0]).to.equal(null);
        expect(_.keys(deltas)[1]).to.equal('a.c.a');
        expect(_.values(deltas)[1]).to.equal('a');
        expect(_.keys(deltas)[2]).to.equal('a.c.b');
        expect(_.values(deltas)[2]).to.equal('b');
        expect(_.keys(deltas)[3]).to.equal('a.c.c');
        expect(_.values(deltas)[3]).to.equal('c');
        expect(_.keys(deltas)[4]).to.equal('a.c.d');
        expect(_.values(deltas)[4]).to.equal('d');
        expect(_.keys(deltas)[5]).to.equal('a.c.e');
        expect(_.values(deltas)[5]).to.equal('e');
    });
    it('.deltaFlatten - exact token', () => {
        const g: Fragment = new Fragment(testingData['samples']['simple']['doc']['a']['c']['a-e'], 'a.c.a-e');
        const deltas = g.deltaFlatten('');
        expect(_.size(deltas)).to.equal(6);
        expect(_.keys(deltas)[0]).to.equal('a.c.a-e');
        expect(_.values(deltas)[0]).to.equal(null);
        expect(_.keys(deltas)[1]).to.equal('a.c.a');
        expect(_.values(deltas)[1]).to.equal('a');
        expect(_.keys(deltas)[2]).to.equal('a.c.b');
        expect(_.values(deltas)[2]).to.equal('b');
        expect(_.keys(deltas)[3]).to.equal('a.c.c');
        expect(_.values(deltas)[3]).to.equal('c');
        expect(_.keys(deltas)[4]).to.equal('a.c.d');
        expect(_.values(deltas)[4]).to.equal('d');
        expect(_.keys(deltas)[5]).to.equal('a.c.e');
        expect(_.values(deltas)[5]).to.equal('e');
    });
    it('.validatePath - exact match', () => {
        const g: Fragment = new Fragment(testingData['samples']['simple']['doc']);
        expect(g.validatePath('a')).to.equal('a');
        expect(g.validatePath('a.c.a-e')).to.equal('a.c.a-e');
        expect(g.validatePath('a.c.a-e.c.b')).to.equal('a.c.a-e.c.b');
    });
    it('.validatePath - no match', () => {
        const g: Fragment = new Fragment(testingData['samples']['simple']['doc']);
        expect(g.validatePath('x')).to.equal(null);
        expect(g.validatePath('x.c.a-e')).to.equal(null);
        expect(g.validatePath('a.c.a-e.x.b')).to.equal(null);
        expect(g.validatePath('a.c.a-e.c.x')).to.equal(null);
    });
    it ('.validatePath - exploded', () => {
        const g: Fragment = new Fragment(testingData['samples']['simple']['doc']);
        expect(g.validatePath('a.c.b')).to.equal('a.c.a-e.c.b');
    });
});
